package demo.authdeposit.api.v1;

import java.util.List;
import java.util.Random;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.TemplateVariable;
import org.springframework.hateoas.UriTemplate;
import org.springframework.stereotype.Component;

import demo.authdeposit.domain.AuthDeposit;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class AuthDepositResourceAssembler implements ResourceAssembler<AuthDeposit, Resource<AuthDeposit>> {

    private final DiscoveryClient discoveryClient;

    public AuthDepositResourceAssembler(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    /**
     * Returns a hypermedia enriched {@link AuthDeposit} entity, or {@code null} if {@code authDeposit} is null.
     */
    @Override
    public Resource<AuthDeposit> toResource(AuthDeposit authDeposit) {
        if (authDeposit == null) {
            return null;
        }
        // Add commands link
        if (!authDeposit.hasLink("commands")) {
            authDeposit.add(linkTo(methodOn(AuthDepositController.class).getAuthDepositCommands(authDeposit.getIdentity()))
                    .withRel("commands"));
        }
        // Add events link
        if (!authDeposit.hasLink("events")) {
            authDeposit.add(linkTo(methodOn(AuthDepositController.class).getAuthDepositEvents(authDeposit.getIdentity()))
                    .withRel("events"));
        }
        // Add remote account link
        if (authDeposit.getAccountId() != null && !authDeposit.hasLink("account")) {
            Link link = getRemoteLink("account-api", "/v1/accounts/{id}",
                    authDeposit.getAccountId(), "account");
            if (link != null) {
                authDeposit.add(link);
            }
        }
        // Add remote connection link
        if (authDeposit.getConnection() != null && !authDeposit.hasLink("connection")) {
            Link link = getRemoteLink("connection-api", "/v1/connections/{id}",
                    authDeposit.getConnection().getIdentity(), "connection");
            if (link != null) {
                authDeposit.add(link);
            }
        }

        return new Resource<>(authDeposit);
    }

    private Link getRemoteLink(String service, String relative, Object identifier, String rel) {
        Link result = null;
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances(service);
        if (serviceInstances.size() > 0) {
            ServiceInstance serviceInstance = serviceInstances.get(new Random().nextInt(serviceInstances.size()));
            result = new Link(new UriTemplate(serviceInstance.getUri()
                    .toString()
                    .concat(relative)).with("id", TemplateVariable.VariableType.PATH_VARIABLE)
                    .expand(identifier)
                    .toString())
                    .withRel(rel);
        }
        return result;
    }
}
