package demo.authdeposit.api.v1;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import demo.authdeposit.exception.AuthDepositNotFoundException;

@ControllerAdvice class AuthDepositNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(AuthDepositNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String authDepositNotFoundHandler(AuthDepositNotFoundException exception) {
        return exception.getMessage();
    }
}
