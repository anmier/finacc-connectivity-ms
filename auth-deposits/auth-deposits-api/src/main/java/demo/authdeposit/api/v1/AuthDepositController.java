package demo.authdeposit.api.v1;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.annotation.Nullable;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositService;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositEvent;
import demo.authdeposit.exception.AuthDepositNotFoundException;
import demo.domain.eventdriven.event.EventService;
import demo.domain.eventdriven.event.Events;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/v1")
public class AuthDepositController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthDepositController.class);

    private final AuthDepositService authDepositService;
    private final EventService<AuthDepositEvent, Long> eventService;
    private final AuthDepositResourceAssembler resourceAssembler;

    public AuthDepositController(AuthDepositService authDepositService, EventService<AuthDepositEvent, Long> eventService,
            AuthDepositResourceAssembler resourceAssembler) {
        this.authDepositService = authDepositService;
        this.eventService = eventService;
        this.resourceAssembler = resourceAssembler;
    }

    @PostMapping(path = "/authDeposits")
    public ResponseEntity createAuthDeposit(@RequestBody AuthDeposit authDeposit) {
        return Optional.ofNullable(createAuthDepositResource(authDeposit))
                .map(resource -> ResponseEntity
                        .created(getURI(resource))
                        .body(resource))
                .orElseThrow(() -> new RuntimeException("Auth Deposit creation failed"));
    }

    @PutMapping(path = "/authDeposits/{id}")
    public ResponseEntity updateAuthDeposit(@RequestBody AuthDeposit authDeposit, @PathVariable Long id) {
        return Optional.ofNullable(updateAuthDepositResource(authDeposit, id))
                .map(resource -> ResponseEntity
                        .created(getURI(resource))
                        .body(resource))
                .orElseThrow(() -> new RuntimeException("Auth Deposit update failed"));
    }

    @RequestMapping(path = "/authDeposits/{id}")
    public Resource<AuthDeposit> getAuthDeposit(@PathVariable Long id) {
        return authDepositService.get(id)
                .map(resourceAssembler::toResource)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @DeleteMapping(path = "/authDeposits/{id}")
    public ResponseEntity deleteAuthDeposit(@PathVariable Long id) {
        authDepositService.delete(id);
        return ResponseEntity
                .noContent()
                .build();
    }

    @RequestMapping(path = "/authDeposits/{id}/events")
    public ResponseEntity getAuthDepositEvents(@PathVariable Long id) {
        return Optional.of(getEventResources(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Could not get events for Auth Deposit " + id));
    }

    @RequestMapping(path = "/authDeposits/{id}/events/{eventId}")
    public ResponseEntity getAuthDepositEvent(@PathVariable Long id, @PathVariable Long eventId) {
        return Optional.of(getEventResource(eventId))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Could not get Auth Deposit event " + eventId));
    }

    @PostMapping(path = "/authDeposits/{id}/events")
    public ResponseEntity createAuthDepositEvent(@PathVariable Long id, @RequestBody AuthDepositEvent event) {
        Resource<AuthDepositEvent> eventResource = appendEventResource(id, event);
        return ResponseEntity
                .created(getURI(eventResource))
                .body(eventResource);
    }

    @RequestMapping(path = "/authDeposits/{id}/commands")
    public ResponseEntity getAuthDepositCommands(@PathVariable Long id) {
        return Optional.of(getCommandsResources(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Could not get commands for Auth Deposit " + id));
    }

    @RequestMapping(path = "/authDeposits/{id}/commands/addConnection")
    public ResponseEntity addConnection(@PathVariable Long id, @RequestParam(value = "connectionId")
            Long connectionId) {
        return authDepositService.get(id)
                .map(authDeposit -> authDeposit.addConnection(connectionId))
                .map(resourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @RequestMapping(path = "/authDeposits/{id}/commands/sendMicrodeposits")
    public ResponseEntity sendMicrodeposits(@PathVariable Long id) {
        return authDepositService.get(id)
                .map(AuthDeposit::sendMicrodeposits)
                .map(resourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @RequestMapping(path = "/authDeposits/{id}/commands/verifyMicrodeposits")
    public ResponseEntity verifyMicrodeposits(@PathVariable Long id) {
        return authDepositService.get(id)
                .map(AuthDeposit::verifyMicrodeposits)
                .map(resourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @RequestMapping(path = "/authDeposits/{id}/commands/updateAuthDepositState")
    public ResponseEntity updateAuthDepositState(@PathVariable Long id, @RequestParam(value = "state")
            AuthDepositVerificationState state) {
        return authDepositService.get(id)
                .map(authDeposit -> authDeposit.updateAuthDepositState(state))
                .map(resourceAssembler::toResource)
                .map(ResponseEntity::ok)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @RequestMapping(path = "/authDeposits/search/findAuthDepositByAccountId")
    public Resource<AuthDeposit> findAuthDepositByAccountId(@RequestParam("accountId") Long accountId) {
        return authDepositService.getAuthDepositByAccountId(accountId)
                .map(resourceAssembler::toResource)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @RequestMapping(path = "/authDeposits/search/findAuthDepositByConnectionId")
    public Resource<AuthDeposit> findAuthDepositByConnectionId(@RequestParam("connectionId") Long connectionId) {
        return authDepositService.getAuthDepositByAccountId(connectionId)
                .map(resourceAssembler::toResource)
                .orElseThrow(AuthDepositNotFoundException::new);
    }

    @Nullable
    private Resource<AuthDeposit> createAuthDepositResource(@NonNull AuthDeposit authDeposit) {
        return resourceAssembler.toResource(authDepositService.registerWithEventFlow(authDeposit));
    }

    @Nullable
    private Resource<AuthDeposit> updateAuthDepositResource(@NonNull AuthDeposit authDeposit, Long id) {
        AuthDeposit authDepositToSave = authDepositService.get(id)
                .map(deposit -> deposit.setFrom(authDeposit))
                .orElseGet(() -> {
                    authDeposit.setIdentity(id);
                    return authDeposit;
                });
        return resourceAssembler.toResource(authDepositService.update(authDepositToSave));
    }

    private Resource<AuthDepositEvent> appendEventResource(Long authDepositId, AuthDepositEvent event) {
        checkNotNull(event, "Event body must be provided");
        AuthDeposit authDeposit = authDepositService.get(authDepositId)
                .orElseThrow(AuthDepositNotFoundException::new);
        event.setEntity(authDeposit);
        // Send AuthDepositEvent
        authDeposit.sendAsyncEvent(event);
        LOGGER.warn("Appending Auth Deposit events");
        return new Resource<>(event,
                linkTo(AuthDepositController.class)
                        .slash("authDeposits")
                        .slash(authDepositId)
                        .slash("events")
                        .slash(event.getEventId())
                        .withSelfRel(),
                linkTo(AuthDepositController.class)
                        .slash("authDeposits")
                        .slash(authDepositId)
                        .withRel("authDeposit")
        );
    }

    private Optional<AuthDepositEvent> getEventResource(Long eventId) {
        return eventService.findOne(eventId);
    }

    private Events getEventResources(Long id) {
        return eventService.find(id);
    }

    private ResourceSupport getCommandsResources(Long id) {
        AuthDeposit authDeposit = AuthDeposit.builder().id(id).build();
        return new Resource<>(authDeposit.getCommands());
    }

    private URI getURI(Resource<? extends ResourceSupport> resource) {
        URI uri = null;
        try {
            uri = new URI(resource.getId().expand().getHref());
        } catch (URISyntaxException e) {
            LOGGER.error("Wrong URI syntax", new IllegalStateException("Error creating URI for resource"
                    + resource.toString()));
        }
        return uri;
    }
}
