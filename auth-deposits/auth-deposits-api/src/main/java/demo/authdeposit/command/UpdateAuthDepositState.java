package demo.authdeposit.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositService;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.domain.eventdriven.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Transactional
public class UpdateAuthDepositState extends Command<AuthDeposit> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAuthDepositState.class);

    private final AuthDepositService authDepositService;

    public UpdateAuthDepositState(AuthDepositService authDepositService) {
        this.authDepositService = authDepositService;
    }

    public AuthDeposit execute(AuthDeposit authDeposit, AuthDepositVerificationState state) {
        // Save rollback state
        AuthDepositVerificationState rollbackState = authDeposit.getState();
        try {
            // Update state
            authDeposit.setState(state);
            authDeposit = authDepositService.update(authDeposit);
        } catch (Exception exception) {
            LOGGER.error("Could not update state", exception);
            authDeposit.setState(rollbackState);
            authDeposit = authDepositService.update(authDeposit);
        }

        return authDeposit;
    }
}
