package demo.authdeposit.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import demo.authdeposit.domain.AuthDeposit;

public interface AuthDepositRepository extends JpaRepository<AuthDeposit, Long> {

    /**
     * Retrieve {@link AuthDeposit}s for a provided {@code accountId}.
     */
    Optional<AuthDeposit> findByAccountId(@Param("accountId") Long accountId);

    /**
     * Retrieve {@link AuthDeposit}s for a provided {@code connection}.
     */
    Optional<AuthDeposit> findByConnectionId(@Param("connectionId") Long connection);
}
