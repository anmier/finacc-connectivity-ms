package demo.authdeposit.event;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;
import demo.authdeposit.api.v1.AuthDepositController;
import demo.authdeposit.domain.AuthDeposit;
import demo.domain.eventdriven.event.Event;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * The {@link AuthDepositEvent} represents domain events produced in the course of auth verification workflow.
 * This event resource can be used to event source the aggregate state of {@link AuthDeposit}.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = {@Index(name = "IDX_AUTH_DEPOSIT_EVENT", columnList = "entity_id")})
public class AuthDepositEvent extends Event<AuthDeposit, AuthDepositVerificationEventType, Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long eventId;

    // Embraces state changes for the auth verification workflow
    @Enumerated(EnumType.STRING)
    private AuthDepositVerificationEventType type;

    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JsonIgnore
    private AuthDeposit entity;

    @CreatedDate
    private Long createdAt;

    @LastModifiedDate
    private Long lastModified;

    public AuthDepositEvent() {
    }

    public AuthDepositEvent(AuthDepositVerificationEventType type) {
        this.type = type;
    }

    public AuthDepositEvent(AuthDepositVerificationEventType type, AuthDeposit entity) {
        this.type = type;
        this.entity = entity;
    }

    @Override
    public Long getEventId() {
        return eventId;
    }

    @Override
    public void setEventId(Long id) {
        eventId = id;
    }

    @Override
    public AuthDepositVerificationEventType getType() {
        return type;
    }

    @Override
    public void setType(AuthDepositVerificationEventType type) {
        this.type = type;
    }

    @Override
    public AuthDeposit getEntity() {
        return entity;
    }

    @Override
    public void setEntity(AuthDeposit entity) {
        this.entity = entity;
    }

    @Override
    public Long getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Long getLastModified() {
        return lastModified;
    }

    @Override
    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public Link getId() {
        return linkTo(AuthDepositController.class)
                .slash("auth_deposits")
                .slash(getEntity().getIdentity())
                .slash("events")
                .slash(getEventId()).withSelfRel();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("eventId", eventId)
                .add("type", type)
                .add("entity", entity)
                .add("createdAt", createdAt)
                .add("lastModified", lastModified)
                .toString();
    }
}
