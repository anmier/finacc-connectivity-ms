package demo.authdeposit.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import demo.authdeposit.api.v1.AuthDepositController;
import demo.authdeposit.command.AddConnection;
import demo.authdeposit.command.SendMicrodeposits;
import demo.authdeposit.command.UpdateAuthDepositState;
import demo.authdeposit.command.VerifyMicrodeposits;
import demo.authdeposit.event.AuthDepositEvent;
import demo.connection.domain.Connection;
import demo.domain.eventdriven.AbstractEventDrivenEntity;
import demo.domain.eventdriven.Aggregate;
import demo.domain.eventdriven.AggregateServiceProvider;
import lombok.Builder;
import lombok.NonNull;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Auth Deposits enable users to automatically verify that microdeposits
 * have cleared at a specified bank connection.
 */
@Builder
@Entity(name = "auth_deposits")
public class AuthDeposit extends AbstractEventDrivenEntity<AuthDepositEvent, Long> {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * Represents state transitions in the course of auth deposit verification workflow.
     * <p/>
     * The aggregate state of a {@link AuthDeposit} is sourced from {@link demo.authdeposit.event.AuthDepositEvent}.
     */
    @Enumerated(value = EnumType.STRING)
    private AuthDepositVerificationState state;

    /**
     * A connection is a representation of the link between end user and a specific financial institution.
     */
    @JsonIgnore
    @OneToOne
    private Connection connection;

    /**
     * A financial account for which the current Autoverified Auth Deposit
     * has been initiated.
     */
    private Long accountId;

    @JsonProperty("authDepositId")
    @Override
    public Long getIdentity() {
        return this.id;
    }

    @Override
    public void setIdentity(Long id) {
        this.id = id;
    }

    public AuthDepositVerificationState getState() {
        return state;
    }

    public void setState(AuthDepositVerificationState state) {
        this.state = state;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    // Methods to modify AuthDeposit state through commands

    public AuthDeposit addConnection(Long connectionId) {
        return getCommand(AddConnection.class)
                .execute(this, connectionId);
    }

    public AuthDeposit sendMicrodeposits() {
        return getCommand(SendMicrodeposits.class)
                .execute(this);
    }

    public AuthDeposit verifyMicrodeposits() {
        return getCommand(VerifyMicrodeposits.class)
                .execute(this);
    }

    public AuthDeposit updateAuthDepositState(AuthDepositVerificationState authDepositState) {
        return getCommand(UpdateAuthDepositState.class)
                .execute(this, authDepositState);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AggregateServiceProvider<A>, A extends Aggregate<AuthDepositEvent, Long>> T getServiceProvider() throws
            IllegalArgumentException {
        AuthDepositServiceProvider authDepositModule = getServiceProvider(AuthDepositServiceProvider.class);
        return (T) authDepositModule;
    }

    /**
     * Returns the {@link Link} with a rel of {@link Link#REL_SELF}.
     */
    @Override
    public Link getId() {
        return linkTo(AuthDepositController.class)
                .slash("authDeposits")
                .slash(getIdentity())
                .withSelfRel();
    }

    public AuthDeposit setFrom(@NonNull AuthDeposit other) {
        this.setAccountId(other.getAccountId());
        this.setConnection(other.getConnection());
        this.setState(other.getState());
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("state", state)
                .add("connection", connection)
                .add("accountId", accountId)
                .toString();
    }
}
