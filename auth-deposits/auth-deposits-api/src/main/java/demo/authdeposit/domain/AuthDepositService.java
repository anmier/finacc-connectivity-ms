package demo.authdeposit.domain;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.authdeposit.event.AuthDepositEvent;
import demo.authdeposit.exception.AuthDepositNotFoundException;
import demo.authdeposit.repository.AuthDepositRepository;
import demo.domain.eventdriven.CrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static demo.authdeposit.event.AuthDepositVerificationEventType.AUTH_DEPOSIT_INITIATED;

/**
 * {@link CrudService} implementation for {@link AuthDeposit} domain object.
 */
@Service
public class AuthDepositService extends CrudService<AuthDeposit, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthDepositService.class);

    private final AuthDepositRepository authDepositRepository;

    public AuthDepositService(AuthDepositRepository authDepositRepository) {
        this.authDepositRepository = authDepositRepository;
    }

    /**
     * Register a new {@link AuthDeposit} and trigger an event flow for auth deposit creation.
     *
     * @param authDeposit is the {@link AuthDeposit} to create
     *
     * @return the created authDeposit
     * @throws IllegalStateException if the event flow fails
     */
    public AuthDeposit registerWithEventFlow(AuthDeposit authDeposit) {
        checkNotNull(authDeposit, "authDeposit cannot be null");
        LOGGER.debug("Registering auth deposit: {}", authDeposit);
        authDeposit = create(authDeposit);
        try {
            authDeposit.sendAsyncEvent(new AuthDepositEvent(AUTH_DEPOSIT_INITIATED, authDeposit));
        } catch (Exception exception) {
            // Rollback creation
            delete(authDeposit.getIdentity());
            throw exception;
        }
        return authDeposit;
    }

    /**
     * Create a new {@link AuthDeposit} entity.
     *
     * @param authDeposit is the {@link AuthDeposit} to create
     *
     * @return the newly created {@link AuthDeposit}
     */
    public AuthDeposit create(AuthDeposit authDeposit) {
        checkNotNull(authDeposit, "authDeposit can't be null");
        // Assert uniqueness constraint
        Long accountId = authDeposit.getAccountId();
        checkState(!authDepositRepository.findByAccountId(accountId).isPresent(),
                "An auth deposit with account id" + accountId + "already exists");
        LOGGER.debug("Creating auth deposit: {}", authDeposit);
        // Save the deposit to the repository
        authDeposit = authDepositRepository.saveAndFlush(authDeposit);
        return authDeposit;
    }

    /**
     * Get an {@link AuthDeposit} entity by identifier.
     *
     * @param id is the unique identifier of a {@link AuthDeposit} entity
     *
     * @return an {@link AuthDeposit} entity
     */
    public Optional<AuthDeposit> get(Long id) {
        LOGGER.debug("Retrieving auth deposit with id: {}", id);
        return authDepositRepository.findById(id);
    }

    /**
     * Get an {@link AuthDeposit} entity by account identifier.
     *
     * @param accountId is the unique identifier of an account
     *
     * @return an {@link AuthDeposit} entity
     */
    public Optional<AuthDeposit> getAuthDepositByAccountId(Long accountId) {
        LOGGER.debug("Retrieving auth deposit by account id: {}", accountId);
        return authDepositRepository.findByAccountId(accountId);
    }

    /**
     * Get an {@link AuthDeposit} entity by account identifier.
     *
     * @param connectionId is the unique identifier of a connection
     *
     * @return an {@link AuthDeposit} entity
     */
    public Optional<AuthDeposit> getAuthDepositByConnectionId(Long connectionId) {
        LOGGER.debug("Retrieving auth deposit by connection id: {}", connectionId);
        return authDepositRepository.findByConnectionId(connectionId);
    }

    /**
     * Update an {@link AuthDeposit} entity with the supplied identifier.
     *
     * @param authDeposit is the {@link AuthDeposit} containing updated fields
     *
     * @return the updated {@link AuthDeposit} entity
     */
    @Transactional
    public AuthDeposit update(AuthDeposit authDeposit) {
        checkNotNull(authDeposit, "authDeposit cannot be null");
        Long id = authDeposit.getIdentity();
        checkNotNull(id, "authDeposit id must be present in the resource URL");
        LOGGER.debug("Updating auth deposit with id: {}", id);
        authDepositRepository.findById(id)
                .orElseThrow(AuthDepositNotFoundException::new);
        return authDepositRepository.saveAndFlush(authDeposit);
    }

    /**
     * Delete the {@link AuthDeposit} with the supplied identifier.
     *
     * @param id is the unique identifier for the {@link AuthDeposit}
     */
    public boolean delete(Long id) {
        LOGGER.debug("Deleting auth deposit with id: {}", id);
        authDepositRepository.findById(id)
                .orElseThrow(AuthDepositNotFoundException::new);
        this.authDepositRepository.deleteById(id);
        return true;
    }
}
