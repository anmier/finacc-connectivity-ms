package demo.authdeposit.domain;

/**
 * The {@link AuthDepositVerificationState} describes state transitions in the course of auth deposit verification workflow.
 */
public enum AuthDepositVerificationState {
    /**
     * The microdeposits could not be sent properly.
     */
    AUTH_DEPOSIT_FAILED,

    /**
     * The microdeposits are about to be sent through the network.
     */
    AUTH_DEPOSIT_INITIATED,

    /**
     * A request for autoverified auth deposits has been made.
     */
    AUTH_DEPOSIT_REQUESTED,

    /**
     * The microdeposits have been sent.
     */
    AUTH_DEPOSIT_SUBMITTED,

    /**
     * The microdeposits have been successfully transferred to the target account.
     */
    AUTH_DEPOSIT_TRANSFERRED,

    /**
     * The deposits could not have been verified within a reasonable time frame.
     */
    AUTH_DEPOSIT_VERIFICATION_FAILED,

    /**
     * The Auth Deposits have been confirmed.
     */
    AUTH_DEPOSIT_VERIFIED,

    /**
     * Connection for an auth deposit has been initiated.
     */
    CONNECTION_INITIATED,

    /**
     * Connection for an auth deposit has been successfully cretaed..
     */
    CONNECTION_CREATED,

    /**
     * Connection for an auth deposit has been failed.
     */
    CONNECTION_FAILED
}
