package demo.domain.eventdriven.event;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class EventController<T extends Event, ID extends Serializable> {

    private final EventService<T, Long> eventService;

    public EventController(EventService<T, Long> eventService) {
        this.eventService = eventService;
    }

    @PostMapping(path = "/events/{id}")
    public ResponseEntity createEvent(@RequestBody T event, @PathVariable Long id) {
        return Optional.ofNullable(eventService.save(id, event))
                .map(e -> new ResponseEntity<>(e, HttpStatus.CREATED))
                .orElseThrow(() -> new RuntimeException("Event creation failed"));
    }

    @PutMapping(path = "/events/{id}")
    public ResponseEntity updateEvent(@RequestBody T event, @PathVariable Long id) {
        return Optional.ofNullable(eventService.save(id, event))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new RuntimeException("Event update failed"));
    }

    @GetMapping(path = "/events/{id}")
    public ResponseEntity getEvent(@PathVariable Long id) {
        return eventService.findOne(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity
                        .notFound()
                        .build());
    }
}
