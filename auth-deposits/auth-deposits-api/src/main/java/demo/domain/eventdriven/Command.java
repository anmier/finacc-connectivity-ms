package demo.domain.eventdriven;

import java.util.Map;
import java.util.function.Consumer;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * A {@code Command} encapsulates a triggered action performed on an {@link Aggregate}.
 * {@code Command}s are meant to drive state changes to domain data.
 */
@Component
public abstract class Command<A extends Aggregate> implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    protected Consumer<A> onSuccess(Map<String, Object> context) {
        return a -> {
        };
    }

    protected Consumer<A> onError(Map<String, Object> context) {
        return a -> {
        };
    }
}
