package demo.domain.eventdriven;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.google.common.base.MoreObjects;
import demo.domain.eventdriven.event.Event;

/**
 * Base event-driven persistent entity that populates its state by application of event series.
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEventDrivenEntity<E extends Event, T extends Serializable> extends Aggregate<E, T>
        implements Serializable {

    private T identity;

    @CreatedDate
    private Long createdAt;

    @LastModifiedDate
    private Long lastModified;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<E> events = new ArrayList<>();

    public AbstractEventDrivenEntity() {
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public List<E> getEvents() {
        return events;
    }

    public void setEvents(List<E> events) {
        this.events = events;
    }

    @Override
    public T getIdentity() {
        return identity;
    }

    public void setIdentity(T id) {
        this.identity = id;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("identity", identity)
                .add("createdAt", createdAt)
                .add("lastModified", lastModified)
                .add("events", events)
                .toString();
    }
}
