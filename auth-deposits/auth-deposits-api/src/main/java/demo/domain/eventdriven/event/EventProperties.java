package demo.domain.eventdriven.event;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "event")
public class EventProperties {

    @NestedConfigurationProperty
    private Props properties;

    public Props getProperties() {
        return properties;
    }

    public void setProperties(Props properties) {
        this.properties = properties;
    }

    public static class Props {

    }
}
