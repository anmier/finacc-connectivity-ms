package demo.domain.eventdriven;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import demo.domain.IdentifiableEntity;
import demo.domain.eventdriven.event.Event;
import demo.domain.eventdriven.event.EventService;

/**
 * An {@link Aggregate} is an entity that populates its state by application of event series,
 * the latter generated via exposed {@link Aggregate}'s commands.
 */
public abstract class Aggregate<E extends Event, ID extends Serializable> extends ResourceSupport implements
        IdentifiableEntity<Link> {

    @JsonProperty("id")
    public abstract ID getIdentity();

    private final ApplicationContext applicationContext = Optional.ofNullable(AggregateServiceProvider.getApplicationContext())
            .orElse(null);

    /**
     * Gets a {@link Command} for this {@link AggregateServiceProvider}.
     *
     * @return the command for this provider
     * @throws IllegalArgumentException if the application context is unavailable or the provider does not exist
     */
    @SuppressWarnings("unchecked")
    @JsonIgnore
    protected <T extends Command<A>, A extends Aggregate> T getCommand(Class<T> commandType) throws IllegalArgumentException {
        AggregateServiceProvider provider = getServiceProvider();
        CrudService service = provider.getDefaultService();
        return (T) service.getCommand(commandType);
    }

    /**
     * Gets an instance of the {@link AggregateServiceProvider} for this instance.
     *
     * @return the provider for this instance
     * @throws IllegalArgumentException if the application context is unavailable or the provider does not exist
     */
    @SuppressWarnings("unchecked")
    @JsonIgnore
    public <T extends demo.domain.eventdriven.AggregateServiceProvider<A>, A extends Aggregate<E, ID>> T getServiceProvider()
            throws IllegalArgumentException {
        return getServiceProvider((Class<T>) ResolvableType
                .forClassWithGenerics(AggregateServiceProvider.class, ResolvableType.forInstance(this))
                .getRawClass());
    }

    /**
     * Gets an instance of a {@link AggregateServiceProvider} for the given type.
     *
     * @return an instance of the requested {@link AggregateServiceProvider}
     * @throws IllegalArgumentException if the application context is unavailable or the provider does not exist
     */
    @JsonIgnore
    public <T extends AggregateServiceProvider<A>, A extends Aggregate<E, ID>> T getServiceProvider(Class<T> providerType)
            throws IllegalArgumentException {
        Assert.notNull(applicationContext, "The application context is unavailable");
        T provider = applicationContext.getBean(providerType);
        Assert.notNull(provider, "The requested provider is not registered in the application context");
        return (T) provider;
    }

    @JsonIgnore
    public abstract List<E> getEvents();

    /**
     * Append a new {@link Event} to the {@link Aggregate} reference for the supplied identifier.
     *
     * @param event is the {@link Event} to append to the {@link Aggregate} entity
     *
     * @return the newly appended {@link Event}
     */

    public E sendEvent(E event, Link... links) {
        return getEventService().send(addEvent(event), links);
    }

    /**
     * Add and apply a new {@link Event} to the {@link Aggregate}.
     */
    public boolean sendAsyncEvent(E event, Link... links) {
        return getEventService().sendAsync(addEvent(event), links);
    }

    /**
     * Adds {@code event} to aggregate's events and saves it via {@link EventService}.
     */
    @Transactional
    @SuppressWarnings("unchecked")
    public E addEvent(E event) {
        event.setEntity(this);
        getEventService().save(event);
        getEvents().add(event);
        getEntityService().update(this);
        return event;
    }

    @Override
    public List<Link> getLinks() {
        List<Link> links = new ArrayList<>(super.getLinks());
        if (!super.hasLink("self")) {
            links.add(getId());
        }

        return links;
    }

    @JsonIgnore
    public CommandResources getCommands() {
        CommandResources commandResources = new CommandResources();
        // FiXME
        return commandResources;
    }

    @SuppressWarnings("unchecked")
    @JsonIgnore
    protected CrudService<Aggregate<E, ID>, ID> getEntityService() {
        return (CrudService<Aggregate<E, ID>, ID>) getServiceProvider().getDefaultService();
    }

    @SuppressWarnings("unchecked")
    @JsonIgnore
    protected EventService<E, ID> getEventService() {
        return (EventService<E, ID>) getServiceProvider().getDefaultEventService();
    }

    public static class CommandResources extends ResourceSupport {

    }
}
