package demo.connection.event;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;
import demo.authdeposit.api.v1.AuthDepositController;
import demo.connection.domain.Connection;
import demo.domain.eventdriven.event.Event;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * The domain event {@link ConnectionEvent} represents events applied to the {@link Connection} domain object.
 * This event resource can be used to event source the aggregate state of {@link Connection}.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = {@Index(name = "IDX_CONNECTION_EVENT", columnList = "entity_id")})
public class ConnectionEvent extends Event<Connection, ConnectionEventType, Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long eventId;

    @Enumerated(EnumType.STRING)
    private ConnectionEventType type;

    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JsonIgnore
    private Connection entity;

    @CreatedDate
    private Long createdAt;

    @LastModifiedDate
    private Long lastModified;

    public ConnectionEvent() {
    }

    public ConnectionEvent(ConnectionEventType type) {
        this.type = type;
    }

    public ConnectionEvent(ConnectionEventType type, Connection entity) {
        this.type = type;
        this.entity = entity;
    }

    @Override
    public Long getEventId() {
        return eventId;
    }

    @Override
    public void setEventId(Long id) {
        eventId = id;
    }

    @Override
    public ConnectionEventType getType() {
        return type;
    }

    @Override
    public void setType(ConnectionEventType type) {
        this.type = type;
    }

    @Override
    public Connection getEntity() {
        return entity;
    }

    @Override
    public void setEntity(Connection entity) {
        this.entity = entity;
    }

    @Override
    public Long getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Long getLastModified() {
        return lastModified;
    }

    @Override
    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public Link getId() {
        // FIXME
        return linkTo(AuthDepositController.class)
                .slash("connections")
                .slash(getEntity().getIdentity())
                .slash("events")
                .slash(getEventId()).withSelfRel();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("eventId", eventId)
                .add("type", type)
                .add("entity", entity)
                .add("createdAt", createdAt)
                .add("lastModified", lastModified)
                .toString();
    }
}
