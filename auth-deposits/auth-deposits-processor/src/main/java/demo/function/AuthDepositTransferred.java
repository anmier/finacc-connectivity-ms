package demo.function;

import java.util.function.Function;

import org.springframework.statemachine.StateContext;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthDepositTransferred extends BaseAuthDepositFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthDepositTransferred.class);

    public AuthDepositTransferred(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context) {
        this(context, null);
    }

    public AuthDepositTransferred(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context,
            Function<AuthDepositVerificationEvent, AuthDeposit> function) {
        super(context, function);
    }

    /**
     * Applies the {@link AuthDepositVerificationEvent} to the {@link AuthDeposit} aggregate.
     *
     * @param event is the {@link AuthDepositVerificationEvent} for this context
     */
    @Override
    public AuthDeposit apply(AuthDepositVerificationEvent event) {
        LOGGER.info("Executing workflow for transferred auth deposit...");
        return super.apply(event);
    }
}
