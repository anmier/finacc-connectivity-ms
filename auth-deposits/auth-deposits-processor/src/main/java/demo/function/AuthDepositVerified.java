package demo.function;

import java.net.URI;
import java.util.function.Function;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.statemachine.StateContext;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthDepositVerified extends BaseAuthDepositFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthDepositVerified.class);

    public AuthDepositVerified(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context) {
        this(context, null);
    }

    public AuthDepositVerified(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context,
            Function<AuthDepositVerificationEvent, AuthDeposit> function) {
        super(context, function);
    }

    /**
     * Applies the {@link AuthDepositVerificationEvent} to the {@link AuthDeposit} aggregate.
     *
     * @param event is the {@link AuthDepositVerificationEvent} for this context
     */
    @Override
    public AuthDeposit apply(AuthDepositVerificationEvent event) {
        LOGGER.info("Executing workflow for verified auth deposit...");
        // TODO: any actions? error description
        // Create a traverson for the root auth deposit
        Traverson traverson = new Traverson(
                URI.create(event.getLink("deposit").getHref()),
                MediaTypes.HAL_JSON
        );
        AuthDeposit authDeposit = traverson.follow("self").toObject(AuthDeposit.class);
        // Add auth deposit to context
        context.getExtendedState().getVariables().put("deposit", authDeposit);

        return authDeposit;
    }
}
