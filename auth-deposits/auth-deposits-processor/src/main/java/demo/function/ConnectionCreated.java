package demo.function;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.statemachine.StateContext;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import demo.connection.domain.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionCreated extends BaseAuthDepositFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionCreated.class);

    public ConnectionCreated(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context) {
        this(context, null);
    }

    public ConnectionCreated(StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context,
            Function<AuthDepositVerificationEvent, AuthDeposit> function) {
        super(context, function);
    }

    /**
     * Applies the {@link AuthDepositVerificationEvent} to the {@link AuthDeposit} aggregate.
     *
     * @param event is the {@link AuthDepositVerificationEvent} for this context
     */
    @Override
    public AuthDeposit apply(AuthDepositVerificationEvent event) {
        LOGGER.info("Executing workflow for created connection...");
        LOGGER.info(event.getType() + ": " + event.getLink("deposit").getHref());
        // Get the deposit resource for the event
        Traverson authDepositResource = new Traverson(
                URI.create(event.getLink("deposit").getHref()),
                MediaTypes.HAL_JSON
        );
        // Get the connection resource for the event
        Traverson connectionResource = new Traverson(
                URI.create(event.getLink("connection").getHref()),
                MediaTypes.HAL_JSON
        );
        // Make auth deposit refer to a connection
        Map<String, Object> template = new HashMap<>();
        template.put("connectionId", connectionResource
                .follow("self")
                .toObject(Connection.class)
                .getIdentity());
        AuthDeposit authDeposit = authDepositResource
                .follow("commands", "addConnection")
                .withTemplateParameters(template)
                .toObject(AuthDeposit.class);
        // Add auth deposit to context
        context.getExtendedState().getVariables().put("deposit", authDeposit);

        return authDeposit;
    }
}
