package demo.util;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class MapperAssist {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapperAssist.class);
    private ObjectMapper objectMapper;

    public MapperAssist(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public HashMap objectToHashMap(Object object) {
        HashMap map = null;
        try {
            String content = objectMapper.writeValueAsString(object);
            map = objectMapper.readValue(content, HashMap.class);
        } catch (IOException e) {
            LOGGER.error("Error mapping object to map", new RuntimeException());
        }
        return map;
    }
}
