package demo.authdeposit.controller;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.authdeposit.AuthDepositStateManager;
import demo.authdeposit.event.AuthDepositVerificationEvent;

@RestController
@RequestMapping("/v1")
public class EventController {

    private AuthDepositStateManager stateManager;

    public EventController(AuthDepositStateManager stateManager) {
        this.stateManager = stateManager;
    }

    @PostMapping(path = "/events")
    public ResponseEntity handleEvent(@RequestBody AuthDepositVerificationEvent event) {
        return Optional.ofNullable(stateManager.applyEvent(event))
                .map(e -> new ResponseEntity<>(e, HttpStatus.CREATED))
                .orElseThrow(() -> new RuntimeException("Apply event failed"));
    }
}
