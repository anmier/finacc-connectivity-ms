package demo.authdeposit.event.processor;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.context.annotation.Profile;

import demo.authdeposit.AuthDepositStateManager;
import demo.authdeposit.event.AuthDepositVerificationEvent;

/**
 * An event processor that listens for Auth Deposit Verification events as AMQP messages
 * and applies events on {@link demo.authdeposit.domain.AuthDeposit} entity.
 */
@EnableAutoConfiguration
@EnableBinding(Sink.class)
@Profile({"cloud", "development", "docker"})
public class AuthDepositVerificationEventProcessor {

    private AuthDepositStateManager authDepositStateManager;

    public AuthDepositVerificationEventProcessor(AuthDepositStateManager authDepositStateManager) {
        this.authDepositStateManager = authDepositStateManager;
    }

    @StreamListener(Sink.INPUT)
    public void streamListener(AuthDepositVerificationEvent authDepositVerificationEvent) {
        authDepositStateManager.applyEvent(authDepositVerificationEvent);
    }
}
