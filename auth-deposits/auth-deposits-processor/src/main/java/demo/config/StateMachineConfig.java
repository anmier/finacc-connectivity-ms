package demo.config;

import java.net.URI;
import java.util.EnumSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import demo.authdeposit.domain.AuthDeposit;
import demo.authdeposit.domain.AuthDepositVerificationState;
import demo.authdeposit.event.AuthDepositVerificationEvent;
import demo.authdeposit.event.AuthDepositVerificationEventType;
import demo.authdeposit.event.AuthDepositVerificationEvents;
import demo.config.aws.AwsLambdaConfig;
import demo.function.AuthDepositFailed;
import demo.function.AuthDepositSubmitted;
import demo.function.AuthDepositTransferred;
import demo.function.AuthDepositVerificationFailed;
import demo.function.AuthDepositVerified;
import demo.function.BaseAuthDepositFunction;
import demo.function.ConnectionCreated;
import demo.function.ConnectionFailed;
import demo.function.ConnectionInitiated;
import demo.function.lambda.AuthDepositInitiated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Defines a state machine to manage auth deposit verification workflow.
 * <p>
 * A state machine provides a declarative language for describing the state of an {@link AuthDeposit}
 * resource given a sequence of ordered {@link AuthDepositVerificationEvents}.
 */
@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<AuthDepositVerificationState,
        AuthDepositVerificationEventType> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateMachineConfig.class);
    private static final String ENTITY_REL_STRING = "deposit";

    @Autowired
    private AwsLambdaConfig.AwsLambdaInvoker lambdaInvoker;

    /**
     * Configures the initial conditions of a new in-memory {@link StateMachine} for Auth Deposit Verification workflow.
     *
     * @param states is the {@link StateMachineStateConfigurer} used to describe the initial condition
     */
    @Override
    public void configure(StateMachineStateConfigurer<AuthDepositVerificationState,
            AuthDepositVerificationEventType> states) {
        try {
            states.withStates()
                    .initial(AuthDepositVerificationState.AUTH_DEPOSIT_REQUESTED)
                    .states(EnumSet.allOf(AuthDepositVerificationState.class));
        } catch (Exception e) {
            throw new RuntimeException("State machine configuration failed", e);
        }
    }

    /**
     * The {@link BaseAuthDepositFunction} argument is only applied
     * if an {@link AuthDepositVerificationEvent} is provided as a message header in the {@link StateContext}.
     *
     * @param context is the state machine context that may include an {@link AuthDepositVerificationEvent}
     * @param authDepositFunction is the function to apply after the state machine has completed replication
     *
     * @return an {@link AuthDepositVerificationEvent} only if this event has not yet been processed,
     * otherwise returns null
     */
    private AuthDepositVerificationEvent handleEvent(
            StateContext<AuthDepositVerificationState, AuthDepositVerificationEventType> context,
            BaseAuthDepositFunction authDepositFunction) {
        AuthDepositVerificationEvent event = null;
        if (context.getMessageHeader("event") != null) {
            event = context.getMessageHeaders().get("event", AuthDepositVerificationEvent.class);
            LOGGER.info("State replication complete for event: {}", event);
            authDepositFunction.apply(event);
        }

        return event;
    }

    /**
     * Configures the {@link StateMachine} that describes how {@link AuthDepositVerificationEventType} drives the state
     * of an {@link AuthDeposit}.
     * <p>
     * Events are triggered on transitions from a source {@link AuthDepositVerificationState}
     * to a target {@link AuthDepositVerificationState}.
     * An {@link Action} is attached to each transition, which maps to a function that is executed
     * in the context of an {@link AuthDepositVerificationEvent}.
     *
     * @param transitions is the {@link StateMachineTransitionConfigurer} used to describe state transitions
     */
    @Override
    public void configure(StateMachineTransitionConfigurer<AuthDepositVerificationState,
            AuthDepositVerificationEventType> transitions) {
        try {
            transitions.withExternal()
                    .source(AuthDepositVerificationState.AUTH_DEPOSIT_REQUESTED)
                    .target(AuthDepositVerificationState.CONNECTION_INITIATED)
                    .event(AuthDepositVerificationEventType.CONNECTION_INITIATED)
                    .action(connectionInitiated())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.CONNECTION_INITIATED)
                    .target(AuthDepositVerificationState.CONNECTION_CREATED)
                    .event(AuthDepositVerificationEventType.CONNECTION_CREATED)
                    .action(connectionCreated())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.CONNECTION_INITIATED)
                    .target(AuthDepositVerificationState.CONNECTION_FAILED)
                    .event(AuthDepositVerificationEventType.CONNECTION_FAILED)
                    .action(connectionFailed())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.CONNECTION_CREATED)
                    .target(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED)
                    .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_INITIATED)
                    .action(authDepositInitiated())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED)
                    .target(AuthDepositVerificationState.AUTH_DEPOSIT_SUBMITTED)
                    .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_SUBMITTED)
                    .action(authDepositSubmitted())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED)
                    .target(AuthDepositVerificationState.AUTH_DEPOSIT_FAILED)
                    .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_FAILED)
                    .action(authDepositFailed())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.AUTH_DEPOSIT_SUBMITTED)
                    .target(AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED)
                    .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_TRANSFERRED)
                    .action(authDepositTransferred())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED)
                    .target(AuthDepositVerificationState.AUTH_DEPOSIT_VERIFIED)
                    .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_VERIFIED)
                    .action(authDepositVerified())
                    .and()
                    .withExternal()
                    .source(AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED)
                    .target(AuthDepositVerificationState.AUTH_DEPOSIT_VERIFICATION_FAILED)
                    .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_VERIFICATION_FAILED)
                    .action(authDepositVerificationFailed());
        } catch (Exception e) {
            throw new RuntimeException("Could not configure state machine transitions", e);
        }
    }

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> connectionInitiated() {
        return context -> handleEvent(context,
                new ConnectionInitiated(context, event -> {
                    LOGGER.info(event.getType() + ": " + getEntityHref(event));
                    Traverson traverson = getResourceForEvent(event);
                    return traverson.follow("self", "commands", "createConnection")
                            .toObject(AuthDeposit.class);
                }));
    }

    //-----------------------------------------------------------------------------------
    // Handle domain events triggered by actions: a few options to go here
    //-----------------------------------------------------------------------------------

    // First option: map BaseAuthDepositFunction to its action-specific subclass function

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> connectionCreated() {
        return context -> handleEvent(context, new ConnectionCreated(context));
    }

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> connectionFailed() {
        return context -> handleEvent(context, new ConnectionFailed(context));
    }

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> authDepositFailed() {
        return context -> handleEvent(context, new AuthDepositFailed(context));
    }

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> authDepositVerified() {
        return context -> handleEvent(context, new AuthDepositVerified(context));
    }

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> authDepositVerificationFailed() {
        return context -> handleEvent(context, new AuthDepositVerificationFailed(context));
    }

    // Second option: map BaseAuthDepositFunction to aws lambda function

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> authDepositInitiated() {
        return context -> handleEvent(context, new AuthDepositInitiated(context, lambdaInvoker.getLambdaService()));
    }

    // One more option is to map BaseAuthDepositFunction directly to Java 8 lambda function

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> authDepositSubmitted() {
        return context -> handleEvent(context,
                new AuthDepositSubmitted(context, event -> {
                    LOGGER.info(event.getType() + ": " + getEntityHref(event));
                    Traverson traverson = getResourceForEvent(event);
                    return traverson.follow("self")
                            .toEntity(AuthDeposit.class)
                            .getBody();
                }));
    }

    @Bean
    public Action<AuthDepositVerificationState, AuthDepositVerificationEventType> authDepositTransferred() {
        return context -> handleEvent(context,
                new AuthDepositTransferred(context, event -> {
                    LOGGER.info(event.getType() + ": " + getEntityHref(event));
                    Traverson traverson = getResourceForEvent(event);
                    return traverson.follow("self", "commands", "verifyMicrodeposits")
                            .toObject(AuthDeposit.class);
                }));
    }

    private Traverson getResourceForEvent(AuthDepositVerificationEvent event) {
        return new Traverson(
                URI.create(getEntityHref(event)),
                MediaTypes.HAL_JSON
        );
    }

    private String getEntityHref(AuthDepositVerificationEvent event) {
        return event.getLink(ENTITY_REL_STRING).getHref();
    }
}
