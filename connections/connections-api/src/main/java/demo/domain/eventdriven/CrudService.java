package demo.domain.eventdriven;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * Service to perform CRUD operations on an {@link Aggregate}.
 */
@Service
public abstract class CrudService<T extends Aggregate, ID extends Serializable> implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public abstract Optional<T> get(ID id);

    public abstract T create(T entity);

    public abstract T update(T entity);

    public abstract boolean delete(ID id);

    @SuppressWarnings("unchecked")
    public <A extends Command<T>> A getCommand(Class<? extends A> clazz) {
        return applicationContext.getBean(clazz);
    }
}
