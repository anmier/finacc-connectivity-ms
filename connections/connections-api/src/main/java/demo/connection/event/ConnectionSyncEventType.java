package demo.connection.event;

import demo.connection.domain.ConnectionSyncState;

/**
 * The {@link ConnectionSyncEventType} represents events that describe the transitions of
 * {@link ConnectionSyncState} in the course of connection sync workflow.
 */
public enum ConnectionSyncEventType {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED,
    SYNC_STARTED,
    SYNC_SUCCEEDED,
    SYNC_STOPPED
}
