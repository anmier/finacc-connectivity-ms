package demo.connection.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.account.domain.AccountService;
import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionService;
import demo.connection.domain.ConnectionSyncState;
import demo.connection.event.ConnectionEvent;
import demo.domain.eventdriven.Command;
import demo.util.ResourceAssemblerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static demo.connection.event.ConnectionSyncEventType.CONNECTION_CREATED;
import static demo.connection.event.ConnectionSyncEventType.CONNECTION_FAILED;

@Service
@Transactional
// TODO: should be moved to account microservice
public class CreateConnection extends Command<Connection> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateConnection.class);

    private final ConnectionService connectionService;
    private final AccountService accountService;

    public CreateConnection(ConnectionService connectionService, AccountService accountService) {
        this.connectionService = connectionService;
        this.accountService = accountService;
    }

    public Connection execute(Long accountId) {
        // TODO: Obtain connection via third-party service
        Connection connection = accountService.get(accountId)
                .map(account -> connectionService.create(Connection.builder()
                        .account(account)
                        .build()))
                .orElseThrow(() -> new RuntimeException("Not valid accountId provided for connection"));
        // Update the connection state
        connection.setState(ConnectionSyncState.CONNECTION_CREATED);
        connection = connectionService.update(connection);

        try {
            // Trigger connection created event
            ConnectionEvent event = new ConnectionEvent(CONNECTION_CREATED, connection);
            event.add(ResourceAssemblerUtils.addSelfLink(connection, "connection"));
            connection.sendAsyncEvent(event);
        } catch (Exception ex) {
            LOGGER.error("Connection could not be created", ex);
            // Rollback the connection creation
            if (connection.getIdentity() != null) {
                connectionService.delete(connection.getIdentity());
            }
            // Trigger connection failed event
            ConnectionEvent event = new ConnectionEvent(CONNECTION_FAILED, connection);
            event.add(ResourceAssemblerUtils.addSelfLink(connection, "connection"));
            connection.sendAsyncEvent(event);
        }

        return connection;
    }
}
