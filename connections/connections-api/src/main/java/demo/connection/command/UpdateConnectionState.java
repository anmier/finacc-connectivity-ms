package demo.connection.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionService;
import demo.connection.domain.ConnectionSyncState;
import demo.domain.eventdriven.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@Transactional
public class UpdateConnectionState extends Command<Connection> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateConnectionState.class);

    private final ConnectionService authDepositService;

    public UpdateConnectionState(ConnectionService authDepositService) {
        this.authDepositService = authDepositService;
    }

    public Connection execute(Connection connection, ConnectionSyncState state) {
        // Save rollback state
        ConnectionSyncState rollbackState = connection.getState();
        try {
            // Update state
            connection.setState(state);
            connection = authDepositService.update(connection);
        } catch (Exception exception) {
            LOGGER.error("Could not update state", exception);
            connection.setState(rollbackState);
            connection = authDepositService.update(connection);
        }

        return connection;
    }
}
