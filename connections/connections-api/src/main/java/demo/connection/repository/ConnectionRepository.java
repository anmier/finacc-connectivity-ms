package demo.connection.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import demo.connection.domain.Connection;

public interface ConnectionRepository extends JpaRepository<Connection, Long> {

    /**
     * Retrieve {@link Connection}s by a provided {@code accountId}.
     */
    Optional<Connection> findConnectionByAccountId(@Param("accountId") Long accountId);
}
