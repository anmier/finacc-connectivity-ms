package demo.connection.domain;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.connection.event.ConnectionEvent;
import demo.connection.exception.ConnectionNotFoundException;
import demo.connection.repository.ConnectionRepository;
import demo.domain.eventdriven.CrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static demo.connection.event.ConnectionSyncEventType.SYNC_STARTED;

@Service
public class ConnectionService extends CrudService<Connection, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionService.class);

    private final ConnectionRepository connectionRepository;

    public ConnectionService(ConnectionRepository connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    /**
     * Register a new {@link Connection} and trigger an event flow for connection creation.
     *
     * @param connection is the {@link Connection} to create
     *
     * @return the created connection
     * @throws IllegalStateException if the event flow fails
     */
    public Connection registerWithEventFlow(Connection connection) {
        checkNotNull(connection, "connection cannot be null");
        LOGGER.debug("Registering connection: {}", connection);
        connection = create(connection);
        try {
            connection.sendAsyncEvent(new ConnectionEvent(SYNC_STARTED, connection));
        } catch (Exception exception) {
            // Rollback creation
            delete(connection.getIdentity());
            throw exception;
        }
        return connection;
    }

    /**
     * Create a new {@link Connection} entity.
     *
     * @param connection is the {@link Connection} to create
     *
     * @return the newly created {@link Connection}
     */
    public Connection create(Connection connection) {
        checkNotNull(connection, "connection can't be null");
        LOGGER.debug("Creating connection: {}", connection);
        // Save the connection to the repository
        connection = connectionRepository.saveAndFlush(connection);
        return connection;
    }

    /**
     * Get an {@link Connection} entity for the provided identifier.
     *
     * @param id is the unique identifier of a {@link Connection} entity
     *
     * @return an {@link Connection} entity
     */
    public Optional<Connection> get(Long id) {
        LOGGER.debug("Retrieving connection with id: {}", id);
        return connectionRepository.findById(id);
    }

    /**
     * Get an {@link Connection} entity for the provided account id.
     *
     * @param accountId is the unique identifier of an {@link demo.account.domain.Account} entity
     *
     * @return an {@link Connection} entity
     */
    public Optional<Connection> getConnectionByAccountId(Long accountId) {
        LOGGER.debug("Retrieving connection by account id: {}", accountId);
        return connectionRepository.findConnectionByAccountId(accountId);
    }

    /**
     * Update an {@link Connection} entity with the provided identifier.
     *
     * @param connection is the {@link Connection} containing updated fields
     *
     * @return the updated {@link Connection} entity
     */
    @Transactional
    public Connection update(Connection connection) {
        checkNotNull(connection, "connection cannot be null");
        Long id = connection.getIdentity();
        checkNotNull(id, "connection id must be present in the resource URL");
        LOGGER.debug("Updating connection with id: {}", id);
        connectionRepository.findById(id)
                .orElseThrow(ConnectionNotFoundException::new);
        return connectionRepository.saveAndFlush(connection);
    }

    /**
     * Delete the {@link Connection} with the provided identifier.
     *
     * @param id is the unique identifier for the {@link Connection}
     */
    public boolean delete(Long id) {
        LOGGER.debug("Deleting connectoin with id: {}", id);
        connectionRepository.findById(id)
                .orElseThrow(ConnectionNotFoundException::new);
        this.connectionRepository.deleteById(id);
        return true;
    }
}
