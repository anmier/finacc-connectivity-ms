package demo.connection.domain;

/**
 * The {@link ConnectionSyncState} describes state transitions in the course of connection sync workflow,
 */
public enum ConnectionSyncState {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED,
    SYNC_STARTED,
    SYNC_SUCCEEDED,
    SYNC_STOPPED
}
