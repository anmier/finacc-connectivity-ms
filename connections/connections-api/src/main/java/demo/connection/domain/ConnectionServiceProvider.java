package demo.connection.domain;

import org.springframework.stereotype.Service;

import demo.connection.event.ConnectionEvent;
import demo.domain.eventdriven.AggregateServiceProvider;
import demo.domain.eventdriven.event.EventService;

@Service
public class ConnectionServiceProvider extends AggregateServiceProvider<Connection> {

    private final ConnectionService connectionService;
    // private final AccountService authDepositService;
    private final EventService<ConnectionEvent, Long> eventService;

    public ConnectionServiceProvider(ConnectionService connectionService,
            EventService<ConnectionEvent, Long> eventService) {
        this.connectionService = connectionService;
        this.eventService = eventService;
    }

    @Override
    public ConnectionService getDefaultService() {
        return connectionService;
    }

    @Override
    public EventService<ConnectionEvent, Long> getDefaultEventService() {
        return eventService;
    }
}
