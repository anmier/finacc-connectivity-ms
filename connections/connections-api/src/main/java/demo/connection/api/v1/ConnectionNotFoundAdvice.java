package demo.connection.api.v1;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import demo.connection.exception.ConnectionNotFoundException;

@ControllerAdvice class ConnectionNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(ConnectionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String connectionNotFoundHandler(ConnectionNotFoundException exception) {
        return exception.getMessage();
    }
}
