package demo.account.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonProperty;
import demo.account.event.AccountEvent;
import demo.connection.api.v1.ConnectionController;
import demo.domain.eventdriven.AbstractEventDrivenEntity;
import demo.domain.eventdriven.Aggregate;
import demo.domain.eventdriven.AggregateServiceProvider;
import lombok.Builder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Represents an account for a financial institution to be synced,
 * identified by a {@link User} and {@link Institution}.
 */
@Builder
@Entity
public class Account extends AbstractEventDrivenEntity<AccountEvent, Long> {

    @Id
    @GeneratedValue
    private Long id;

    /**
     * User associated with the account.
     */
    @ManyToOne
    private User user;

    /**
     * Financial institution associated with the account.
     */
    @ManyToOne
    private Institution institution;

    @Enumerated(value = EnumType.STRING)
    private AccountCategory accountCategory;

    @JsonProperty("accountId")
    @Override
    public Long getIdentity() {
        return id;
    }

    public void setIdentity(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public AccountCategory getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(AccountCategory accountCategory) {
        this.accountCategory = accountCategory;
    }

    /**
     * Returns the {@link Link} with a rel of {@link Link#REL_SELF}.
     */
    @Override
    public Link getId() {
        return linkTo(ConnectionController.class)
                .slash("accounts")
                .slash(getIdentity())
                .withSelfRel();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends AggregateServiceProvider<A>, A extends Aggregate<AccountEvent, Long>> T getServiceProvider() throws
            IllegalArgumentException {
        AccountServiceProvider connectionModule = getServiceProvider(AccountServiceProvider.class);
        return (T) connectionModule;
    }
}
