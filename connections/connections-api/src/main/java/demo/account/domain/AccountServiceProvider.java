package demo.account.domain;

import org.springframework.stereotype.Service;

import demo.connection.event.ConnectionEvent;
import demo.domain.eventdriven.AggregateServiceProvider;
import demo.domain.eventdriven.event.EventService;

@Service
public class AccountServiceProvider extends AggregateServiceProvider<Account> {

    private final AccountService connectionService;
    private final EventService<ConnectionEvent, Long> eventService;

    public AccountServiceProvider(AccountService connectionService,
            EventService<ConnectionEvent, Long> eventService) {
        this.connectionService = connectionService;
        this.eventService = eventService;
    }

    public AccountService getConnectionService() {
        return connectionService;
    }

    public EventService<ConnectionEvent, Long> getEventService() {
        return eventService;
    }

    @Override
    public AccountService getDefaultService() {
        return connectionService;
    }

    @Override
    public EventService<ConnectionEvent, Long> getDefaultEventService() {
        return eventService;
    }
}
