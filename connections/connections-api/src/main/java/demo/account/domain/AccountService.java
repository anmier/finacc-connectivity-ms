package demo.account.domain;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.hateoas.TemplateVariable;
import org.springframework.hateoas.UriTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.domain.eventdriven.CrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class AccountService extends CrudService<Account, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

    private final RestTemplate restTemplate;

    public AccountService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Optional<Account> get(Long accountId) {
        Account account;
        try {
            account = restTemplate.getForObject(new UriTemplate("http://accounts-api/v1/accounts/{id}")
                    .with("id", TemplateVariable.VariableType.PATH_VARIABLE)
                    .expand(accountId), Account.class);
        } catch (RestClientResponseException exception) {
            LOGGER.error("Get account failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return Optional.ofNullable(account);
    }

    @Override
    public Account create(Account account) {
        Account result;
        try {
            result = restTemplate.postForObject(new UriTemplate("http://accounts-api/v1/accounts").expand(),
                    account, Account.class);
        } catch (RestClientResponseException exception) {
            LOGGER.error("Create account failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return result;
    }

    @Override
    public Account update(Account entity) {
        Account result = null;
        try {
            result = restTemplate.exchange(new RequestEntity<>(entity, HttpMethod.PUT,
                    new UriTemplate("http://accounts-api/v1/accounts/{id}")
                            .with("id", TemplateVariable.VariableType.PATH_VARIABLE)
                            .expand(entity.getIdentity())), Account.class).getBody();
        } catch (RestClientResponseException exception) {
            LOGGER.error("Update account failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return result;
    }

    @Override
    public boolean delete(Long accountId) {
        try {
            restTemplate.delete(new UriTemplate("http://accounts-api/v1/accounts/{id}")
                    .with("id", TemplateVariable.VariableType.PATH_VARIABLE).expand(accountId));
        } catch (RestClientResponseException exception) {
            LOGGER.error("Delete account failed", exception);
            throw new IllegalStateException(getHttpStatusMessage(exception), exception);
        }

        return true;
    }

    private String getHttpStatusMessage(RestClientResponseException exception) {
        Map<String, String> errorMap = new HashMap<>();
        try {
            errorMap = new ObjectMapper().readValue(exception.getResponseBodyAsString(), errorMap.getClass());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return errorMap.getOrDefault("message", null);
    }
}
