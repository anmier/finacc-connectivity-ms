package demo.account.domain;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;

public class Accounts extends Resources<Account> {

    /**
     * Creates a {@link Resources} instance with the given content and {@link Link}s (optional).
     *
     * @param content must not be {@literal null}.
     * @param links the links to be added to the {@link Resources}.
     */
    public Accounts(Iterable<Account> content, Link... links) {
        super(content, links);
    }
}
