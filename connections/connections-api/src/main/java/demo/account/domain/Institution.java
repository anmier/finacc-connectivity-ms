package demo.account.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Represents a financial institution to make a connection to for any particular {@link User}.
 */
@Entity
public class Institution {

    @Id
    @GeneratedValue
    private Long id;
}
