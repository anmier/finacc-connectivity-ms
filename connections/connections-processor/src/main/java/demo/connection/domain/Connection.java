package demo.connection.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.sql.ConnectionEvent;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonProperty;
import demo.domain.HATEOASEntity;

public class Connection extends HATEOASEntity {

    private Long id;
    private Set<ConnectionEvent> events = new HashSet<>();
    private ConnectionState status;
    private Boolean isDisabled;
    private List<Long> accountIds = new ArrayList<>();

    /**
     * Returns the {@link Link} with a rel of {@link Link#REL_SELF}.
     */
    @Override
    public Link getId() {
        return getLink("self");
    }

    @JsonProperty("connectionId")
    public Long getIdentity() {
        return this.id;
    }

    public void setIdentity(Long id) {
        this.id = id;
    }

    public ConnectionState getStatus() {
        return status;
    }

    public void setStatus(ConnectionState status) {
        this.status = status;
    }

    public Boolean getDisabled() {
        return isDisabled;
    }

    public void setDisabled(Boolean disabled) {
        isDisabled = disabled;
    }

    public List<Long> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<Long> accountIds) {
        this.accountIds = accountIds;
    }
}
