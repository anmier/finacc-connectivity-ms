package demo.connection.domain;

public enum ConnectionState {
    CONNECTION_INITIATED,
    CONNECTION_CREATED,
    CONNECTION_FAILED,
    SYNC_STARTED
}
