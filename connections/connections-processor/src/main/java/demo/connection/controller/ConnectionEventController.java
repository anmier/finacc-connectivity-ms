package demo.connection.controller;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.connection.ConnectionStateManager;
import demo.connection.event.ConnectionEvent;

@RestController
@RequestMapping("/v1/connection")
public class ConnectionEventController {

    private ConnectionStateManager eventService;

    public ConnectionEventController(ConnectionStateManager eventService) {
        this.eventService = eventService;
    }

    @PostMapping(path = "/events")
    public ResponseEntity handleEvent(@RequestBody ConnectionEvent event) {
        return Optional.ofNullable(eventService.applyEvent(event))
                .map(e -> new ResponseEntity<>(e, HttpStatus.CREATED))
                .orElseThrow(() -> new RuntimeException("Apply event failed"));
    }
}
