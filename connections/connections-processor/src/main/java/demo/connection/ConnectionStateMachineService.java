package demo.connection;

import java.net.URI;
import java.util.Comparator;
import java.util.UUID;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import demo.connection.domain.ConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionEventType;
import demo.connection.event.ConnectionEvents;
import demo.domain.HATEOASEntity;
import lombok.NonNull;

/**
 * The {@link ConnectionStateMachineService} provides methods to create and start a new state machine
 * and to replicate the state of an {@link demo.connection.domain.Connection} aggregate entity.
 */
@Service
public class ConnectionStateMachineService {

    private final StateMachineFactory<ConnectionState, ConnectionEventType> factory;
    private final RestTemplate restTemplate;

    public ConnectionStateMachineService(StateMachineFactory<ConnectionState, ConnectionEventType> factory,
            RestTemplate restTemplate) {
        this.factory = factory;
        this.restTemplate = restTemplate;
    }

    /**
     * Creates and starts a new state machine.
     */
    public StateMachine<ConnectionState, ConnectionEventType> newStateMachine() {
        StateMachine<ConnectionState, ConnectionEventType> stateMachine =
                factory.getStateMachine(UUID.randomUUID().toString());
        stateMachine.start();
        return stateMachine;
    }

    /**
     * Replicates the state of an {@link java.util.concurrent.locks.Condition} from a sequence of
     * {@link demo.connection.event.ConnectionEvents}s.
     *
     * @param stateMachine state machine for replicating an aggregate's state
     * @param event received event
     *
     * @return state machine instance after replication
     */
    public StateMachine<ConnectionState, ConnectionEventType> replicateState(
            @NonNull StateMachine<ConnectionState, ConnectionEventType> stateMachine,
            @NonNull ConnectionEvent event) {
        Link eventId = event.getId();
        ConnectionEvents eventLog = getEventLog(event);
        // For each event in the log trigger events
        eventLog.getContent().stream()
                .sorted(Comparator.comparing(HATEOASEntity::getCreatedAt))
                .forEach(e -> stateMachine.sendEvent(e.getId().equals(eventId) ?
                        getMessageWithHeaders(e) :
                        getMessageWithPayload(e)));

        return stateMachine;
    }

    public StateMachine<ConnectionState, ConnectionEventType> replicateState(
            @NonNull ConnectionEvent event) {
        StateMachine<ConnectionState, ConnectionEventType> stateMachine = newStateMachine();
        Link eventId = event.getId();
        ConnectionEvents eventLog = getEventLog(event);
        // For each event in the log trigger events
        eventLog.getContent().stream()
                .sorted(Comparator.comparing(HATEOASEntity::getCreatedAt))
                .forEach(e -> stateMachine.sendEvent(e.getId().equals(eventId) ?
                        getMessageWithHeaders(e) :
                        getMessageWithPayload(e)));

        return stateMachine;
    }

    private Message<ConnectionEventType> getMessageWithPayload(@NonNull ConnectionEvent event) {
        return MessageBuilder
                .withPayload(event.getType())
                .build();
    }

    private Message<ConnectionEventType> getMessageWithHeaders(@NonNull ConnectionEvent event) {
        return MessageBuilder
                .withPayload(event.getType())
                .setHeader("event", event)
                .build();
    }

    private ConnectionEvents getEventLog(ConnectionEvent event) {
        URI href = URI.create(event.getLink("connection").getHref());
        Traverson traverson = new Traverson(href, MediaTypes.HAL_JSON);
        traverson.setRestOperations(restTemplate);
        return traverson.follow("events")
                .toEntity(ConnectionEvents.class)
                .getBody();
    }
}
