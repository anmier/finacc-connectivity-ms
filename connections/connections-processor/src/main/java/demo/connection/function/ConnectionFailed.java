package demo.connection.function;

import java.net.URI;
import java.util.function.Function;

import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.statemachine.StateContext;

import demo.connection.domain.Connection;
import demo.connection.domain.ConnectionState;
import demo.connection.event.ConnectionEvent;
import demo.connection.event.ConnectionEventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionFailed extends BaseConnectionFunction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionFailed.class);

    public ConnectionFailed(StateContext<ConnectionState, ConnectionEventType> context) {
        this(context, null);
    }

    public ConnectionFailed(StateContext<ConnectionState, ConnectionEventType> context,
            Function<ConnectionEvent, Connection> function) {
        super(context, function);
    }

    /**
     * Applies the {@link ConnectionEvent} to the {@link Connection} aggregate.
     *
     * @param event is the {@link ConnectionEvent} for this context
     */
    @Override
    public Connection apply(ConnectionEvent event) {
        LOGGER.info("Executing workflow for failed connection...");
        // TODO: add error description
        // Create a traverson for the root connection
        Traverson traverson = new Traverson(
                URI.create(event.getLink("connection").getHref()),
                MediaTypes.HAL_JSON
        );
        Connection connection = traverson.follow("self").toObject(Connection.class);
        // Add auth deposit to context
        context.getExtendedState().getVariables().put("connection", connection);

        return connection;
    }
}
