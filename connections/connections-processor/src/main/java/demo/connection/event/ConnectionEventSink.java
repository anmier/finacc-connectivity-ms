package demo.connection.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface ConnectionEventSink {

    String INPUT = "connection";

    @Input(ConnectionEventSink.INPUT)
    SubscribableChannel input();
}
