package demo.connection.event;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Profile;

import demo.connection.ConnectionStateManager;

/**
 * An event processor that listens for connection events as AMQP messages
 * and applies events on {@link demo.connection.domain.Connection} entity.
 */
@EnableAutoConfiguration
@EnableBinding(ConnectionEventSink.class)
@Profile({"cloud", "development", "docker"})
public class ConnectionEventProcessor {

    private ConnectionStateManager stateManager;

    public ConnectionEventProcessor(ConnectionStateManager stateManager) {
        this.stateManager = stateManager;
    }

    @StreamListener(ConnectionEventSink.INPUT)
    public void streamListener(ConnectionEvent connectionEvent) {
        stateManager.applyEvent(connectionEvent);
    }
}
