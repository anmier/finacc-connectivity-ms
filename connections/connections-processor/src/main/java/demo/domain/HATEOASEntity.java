package demo.domain;

import org.springframework.hateoas.ResourceSupport;

import com.google.common.base.MoreObjects;

public class HATEOASEntity extends ResourceSupport {

    private Long createdAt;
    private Long lastModified;

    public HATEOASEntity() {
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("createdAt", createdAt)
                .add("lastModified", lastModified)
                .toString();
    }
}
