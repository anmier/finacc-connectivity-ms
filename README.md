# Account Connectivity Microservices Demo

This is a work-in-progress demo project inspired by [Quovo](https://www.quovo.com/) domain model and showcasing
a few concepts in building event-driven microservices with Spring Boot and Spring Cloud support:

* Hypermedia event logging
* Event-driven messaging, event sourcing
* Statemachine (SM)
* Serverless Functions

## Some Background And Traditional Terminology

There is a fair amount of interest in '_event-driven architecture_' these days.
The term covers a fair range of ideas, but most of it centers around systems communicating through event messages.

> Such systems can operate in a very loosely coupled parallel style which provides excellent horizontal scalability and
resilience to systems failure.
>
> -- <cite>Martin Fowler</cite>

Event Sourcing ensures that all changes to application state are stored as a sequence of events.
Not just can we query these events, we can also use the event log to reconstruct past states,
and as a foundation to automatically adjust the state to cope with retroactive changes.

The first-class citizens of event-driven applications are [domain events](https://martinfowler.com/eaaDev/DomainEvent.html)
used to capture _events_ that can trigger a change to the state.
As Martin Fowler put it,

> **Domain event** captures information from the external stimulus. Since this is logged and we want to use the log as
> an audit trail, it's important that this **source data is immutable**.

Events are handled by an explicitly mapped function (also termed as **'action'** or **'command'**).
Event processing implies both _synchronous_ (**HTTP-driven**) and _asynchronous_ (**message-driven**) interactions.

## Domain Model

This demo is built around ideas for **_financial connectivity_**, to _empower tracking and analytics of financial
accounts_.

The few bounded contexts provided here deal with **_auth deposits verification_** and **_connection synchronization_**
workflows.

The first, **autoverified auth deposits** present a workflow to help users validate an account by sending
and verifing microdeposits.

**Connection** is a representation of the link between an end user and a specific financial institution.
It’s through this connection that an interface with an institution is provided and the user’s account data is synced.

As Quovo model suggests, _connections_ should be viewed as distinct entities, different than _syncs_,
each providing separate endpoints and enclosed within a self-contained microservice.

> Connections *represent a record linking a user to an institution, while the sync process actually updates the data
> for the user’s account*.

`AuthDeposit` and `Connection` are domain entities, (or _aggregates_) central to the corresponding bounded context,
both inheriting from `AbstractEventDrivenEntity`:

```java
/**
 * Base event-driven persistent entity that populates its state by application of event series.
 */
@MappedSuperclass
@Getter @Setter
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEventDrivenEntity<E extends Event, T extends Serializable> extends Aggregate<E, T>
        implements Serializable {

    private T identity;

    @CreatedDate
    private Long createdAt;

    @LastModifiedDate
    private Long lastModified;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<E> events = new ArrayList<>();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("identity", identity)
                .add("createdAt", createdAt)
                .add("lastModified", lastModified)
                .add("events", events)
                .toString();
    }
}
```

Each of the microservices features submodules matching both types of interactions (HTTP-based and AMQP-based).

## Statemachine and Serverless Functions

Here is a high-level representation of auth deposit verification workflow sourced from Quovo documentation:

![Auth Deposit Verification](https://docs.quovo.com/wp-content/uploads/2018/01/quovo_connection_diagram_avms.png)

As you may already know, Spring’s [State Machine project](http://http://projects.spring.io/spring-statemachine/)
can be in some cases a good fit for workflows or any kind of finite state automata representations.

Below is the sample statemachine configuration to capture events that trigger state transitions for an `AuthDeposit`
domain object:

```java
@Override
public void configure(StateMachineTransitionConfigurer<AuthDepositVerificationState,
        AuthDepositVerificationEventType> transitions) {
    try {
        transitions.withExternal()
                .source(AuthDepositVerificationState.AUTH_DEPOSIT_REQUESTED)
                .target(AuthDepositVerificationState.CONNECTION_INITIATED)
                .event(AuthDepositVerificationEventType.CONNECTION_INITIATED)
                .action(connectionInitiated())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.CONNECTION_INITIATED)
                .target(AuthDepositVerificationState.CONNECTION_CREATED)
                .event(AuthDepositVerificationEventType.CONNECTION_CREATED)
                .action(connectionCreated())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.CONNECTION_INITIATED)
                .target(AuthDepositVerificationState.CONNECTION_FAILED)
                .event(AuthDepositVerificationEventType.CONNECTION_FAILED)
                .action(connectionFailed())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.CONNECTION_CREATED)
                .target(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED)
                .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_INITIATED)
                .action(authDepositInitiated())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED)
                .target(AuthDepositVerificationState.AUTH_DEPOSIT_SUBMITTED)
                .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_SUBMITTED)
                .action(authDepositSubmitted())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.AUTH_DEPOSIT_INITIATED)
                .target(AuthDepositVerificationState.AUTH_DEPOSIT_FAILED)
                .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_FAILED)
                .action(authDepositFailed())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.AUTH_DEPOSIT_SUBMITTED)
                .target(AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED)
                .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_TRANSFERRED)
                .action(authDepositTransferred())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED)
                .target(AuthDepositVerificationState.AUTH_DEPOSIT_VERIFIED)
                .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_VERIFIED)
                .action(authDepositVerified())
                .and()
                .withExternal()
                .source(AuthDepositVerificationState.AUTH_DEPOSIT_TRANSFERRED)
                .target(AuthDepositVerificationState.AUTH_DEPOSIT_VERIFICATION_FAILED)
                .event(AuthDepositVerificationEventType.AUTH_DEPOSIT_VERIFICATION_FAILED)
                .action(authDepositVerificationFailed());
    } catch (Exception e) {
            throw new RuntimeException("Could not configure state machine transitions", e);
    }
}
```

An `Action` is attached to each transition from a source `AuthDepositVerificationState` to a target
`AuthDepositVerificationState` which maps to a function executed in the context of an `AuthDepositVerificationEvent`.

The handler functions mapped to **actions** come in several flavours, one being an [AWS Lambda](https://aws.amazon.com/lambda/)
Function implementation.

The AWS Adapter in `spring-cloud-function-adapters` has a couple of request handlers we can use like
`SpringBootRequestHandler` that propagates the request to our function.

```java
public class SendMicrodepositsFunctionHandler extends SpringBootRequestHandler<AuthDepositVerificationEvent, AuthDeposit> {

}
```

_The only reason we need to implement it is to specify the type of the input and the output parameters of the function,
so AWS can serialize/deserialize them for us._ For more details, please, refer to
[article here](https://dzone.com/articles/run-code-with-spring-cloud-function-on-aws-lambda),
as well as Spring guides and documentation on the subject.

## Prerequisites for Running the Demo

To run this demo locally you need to start:
 
* _Apache Kafka_ (with Zookeper)
* _Discovery service_
* _Data Flow server_
* _Redis_ (optionally)

Note, that you may also need _Redis_ as a dependency with Spring Cloud Data Flow when running any streams that involve
analytics applications.
