package demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.dataflow.rest.client.DataFlowTemplate;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Component
public class StreamDefinitions implements ApplicationListener<ApplicationReadyEvent> {

    @Value("${server.port}")
    private String port;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        URI baseUri = URI.create("http://localhost:" + port);
        DataFlowTemplate dataFlowTemplate = new DataFlowTemplate(baseUri);
        try {
            dataFlowTemplate.appRegistryOperations()
                    .importFromResource(new URL(baseUri.toURL(), "app.properties").toString(), false);
            dataFlowTemplate.appRegistryOperations()
                    .importFromResource("http://bit.ly/stream-applications-kafka-maven", false);
            List<StreamDefinition> streams = Arrays.asList(
                    new StreamDefinition("authdeposit-event-stream",
                            "authdeposit-api > :authdeposit-stream"),
                    new StreamDefinition("authdeposit-event-processor",
                            ":authdeposit-stream > authdeposit-processor"),
                    new StreamDefinition("authdeposit-event-counter",
                            ":authdeposit-stream > field-value-counter --field-name=type --name=authdeposit-events")
            // Todo: Add processors
            );
            streams.parallelStream()
                    .forEach(stream -> dataFlowTemplate.streamOperations()
                            .createStream(stream.getName(), stream.getDefinition(), true));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @AllArgsConstructor
    @Getter @Setter
    static class StreamDefinition {
        private String name;
        private String definition;
    }
}
